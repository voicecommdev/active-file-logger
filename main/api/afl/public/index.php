<?php

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use AFLControllers\ActiveFileLoggerController;
use AFLModels\MyVoiceCommModel;

require __DIR__ . '/../vendor/autoload.php';

// Get credentials from environment
$dotenv = Dotenv\Dotenv::createImmutable('../');
$dotenv->load();

// Create Container using PHP-DI
$container = new Container();

// Inject Service - Model
$container->set('MyVoiceCommModel', function () {
    $hostname = getenv('DB_HOSTNAME');
    $databaseName = getenv('DB_DATABASE');
    $dsn = 'mysql:dbname=' . $databaseName . ';host=' . $hostname;
    $user = getenv('DB_USERNAME');
    $pass = getenv('DB_PASSWORD');
    $pdo = new PDO($dsn, $user, $pass);
    return new MyVoiceCommModel($pdo);
});

// Inject Service - Logger
$container->set('Logger', function () {
    $log = new Logger('AFLLogger');
    $log->pushHandler(new StreamHandler(
        getenv('AFL_LOG_DIRECTORY') . '/' .
        getenv('AFL_LOG_FILE'), Logger::WARNING));
    return $log;
});

// Set container to create App with on AppFactory
AppFactory::setContainer($container);
$app = AppFactory::create();

$app->post('/log-access', ActiveFileLoggerController::class . ':logAccessToFile');

$app->post('/set-marked-files', ActiveFileLoggerController::class . ':setMarkedFiles');

$app->get('/get-marked-files', ActiveFileLoggerController::class . ':getMarkedFiles');

$app->get('/inactive-files', ActiveFileLoggerController::class . ':getInactiveFiles');

$app->run();
