<?php

namespace AFLControllers;

use http\Exception;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use AFLModels\MyVoiceCommModel;

class ActiveFileLoggerController
{
    public MyVoiceCommModel $database;
    public Logger $logger;
    protected $container;

    /**
     * ActiveFileLoggerController constructor.
     * @param ContainerInterface $c
     */
    public function __construct(ContainerInterface $c)
    {
        $this->container = $c;
        $this->database = $this->container->get('MyVoiceCommModel');
        $this->logger = $this->container->get('Logger');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws Exception
     */
    public function logAccessToFile(Request $request, Response $response, $args) {

        try {
            // get data from request
            $body = $request->getParsedBody();
            $file = $body['name'];
            $path = $body['path'];

            if (!isset($file) || !isset($path)) {
                throw new \Exception('Improper Post Body - Relevant Data missing.');
            }

            $result = [ 'log_access' => $this->database->logAccessToFile($file, $path)];
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" .
                "File: " . $e->getFile() . "\r\n" .
                "Line: " . $e->getLine() . "\r\n"
            );
            $response->withStatus(500);
            $result = ['Error' => $e->getMessage()];
        }

        $payload = json_encode($result);
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function getMarkedFiles(Request $request, Response $response, $args){
        try {
            $result = $this->database->getMarkedFiles();

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" .
                "File: " . $e->getFile() . "\r\n" .
                "Line: " . $e->getLine() . "\r\n"
            );
            $response->withStatus(500);
            $result = ['Error' => $e->getMessage()];
        }

        $payload = json_encode($result);
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function setMarkedFiles(Request $request, Response $response, $args){
        try {
            $data = $request->getParsedBody();
            $size = count($data);
            if ($size === 0) {
                throw new \Exception("No data sent");
            }

            $values = [];
            foreach ($data as $key => $filePath) {
                preg_match('/[^\/]+$/', $filePath, $file);
                preg_match('/^.+[^a-zA-Z0-9\-\_+.php+$]/', $filePath, $path);
                array_push($values, $file);
                array_push($values, $path);
            }
            $valueSetsNeeded = count($values) / 2;

            $result = $this->database->setMarkedFiles($values, $valueSetsNeeded);

            if (!$result) {
                throw new \Exception('Marked files not set.');
            }

            $result = ['marked_files_set' => $result];
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" .
                "File: " . $e->getFile() . "\r\n" .
                "Line: " . $e->getLine() . "\r\n"
            );
            $response->withStatus(500);
            $result = ['Error' => $e->getMessage()];
        }

        $payload = json_encode($result);
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function getInactiveFiles(Request $request, Response $response, $args){
        try {
            $result = $this->database->getInactiveFiles();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . "\r\n" .
                "File: " . $e->getFile() . "\r\n" .
                "Line: " . $e->getLine() . "\r\n"
            );
            $response->withStatus(500);
            $result = ['Error' => $e->getMessage()];
        }

        $payload = json_encode($result);
        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }
}