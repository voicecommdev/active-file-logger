<?php

namespace AFLModels;

use PDO;

class MyVoiceCommModel
{
    public PDO $database;

    /**
     * MyVoiceCommModel constructor.
     * @param $connection
     */
    public function __construct(PDO $connection)
    {
        $this->database = $connection;
    }

    /**
     * @param $fileName
     * @param $filePath
     * @return bool
     * @throws Exception
     */
    public function logAccessToFile($fileName, $filePath){
        $statement = $this->database->prepare("INSERT INTO active_files (name, path, date_created) VALUES (?, ?, CURRENT_TIMESTAMP());");
        $result = $statement->execute([$fileName, $filePath]);
        return $result;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function getMarkedFiles(){
        $statement = $this->database->prepare("SELECT name, path FROM marked_files GROUP BY name, path;");
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * @param $data
     * @return bool
     * @throws Exception
     *
     * Appends the given values to the table of marked files.
     */
    public function setMarkedFiles($values, $valueSetsNeeded){
        $sql = "INSERT INTO marked_files (name, path, date_created) VALUES ";
        $valueSet = "(?, ?, CURRENT_TIMESTAMP())";

        for ($i = 0; $i < $valueSetsNeeded; $i++) {
            $sql .= $valueSet;
            $nextIterationFlag = $i+1;

            if ($nextIterationFlag < $valueSetsNeeded) {
                $sql .= ',';
            } else {
                $sql .= ';';
            }
        }

        $statement = $this->database->prepare($sql);
        $counter = 1;
        foreach ($values as $value) {
            $statement->bindValue($counter, $value[0], PDO::PARAM_STR);
            $counter++;
        }
        $result = $statement->execute();
        return $result;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function getInactiveFiles(){
        $statement = $this->database->prepare("SELECT marked.name, marked.path FROM marked_files as marked LEFT JOIN active_files as active ON marked.name = active.name AND marked.path = active.path WHERE active.path IS NULL AND active.name IS NULL GROUP BY name, path;");
        $statement->execute();
        return $statement->fetchAll();
    }
}