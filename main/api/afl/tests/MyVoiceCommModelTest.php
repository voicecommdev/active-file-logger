<?php
use PHPUnit\Framework\TestCase;
use AFLModels\MyVoiceCommModel;

class MyVoiceCommModelTest extends TestCase
{
    public $prophet;
    public $system_under_test;

    public function setUp(): void
    {
        $this->prophet = new \Prophecy\Prophet();
    }

    public function testModelLogAccessToFiles()
    {
        $statementDummy = $this->prophet->prophesize(PDOStatement::class);
        $statementDummy->execute(['file.php', '/path/to/'])->willReturn(true);

        $pdoDummy = $this->prophet->prophesize(PDO::class);
        $pdoDummy
            ->prepare("INSERT INTO active_files (name, path, date_created) VALUES (?, ?, CURRENT_TIMESTAMP());")
            ->willReturn($statementDummy->reveal());

        $this->system_under_test = new MyVoiceCommModel($pdoDummy->reveal());

        $result = $this->system_under_test->logAccessToFile('file.php', '/path/to/');
        $this->assertTrue($result);
    }

    public function testGetMarkedFiles()
    {
        $statementDummy = $this->prophet->prophesize(PDOStatement::class);
        $statementDummy->execute();
        $statementDummy->fetchAll()->willReturn([['name' => 'test.php', 'path' => '/path/to/'], ['name' => 'test.php', 'path' => '/path/to/']]);

        $pdoDummy = $this->prophet->prophesize(PDO::class);
        $pdoDummy
            ->prepare("SELECT name, path FROM marked_files GROUP BY name, path;")
            ->willReturn($statementDummy->reveal());

        $this->system_under_test = new MyVoiceCommModel($pdoDummy->reveal());

        $result = $this->system_under_test->getMarkedFiles();

        // Similar method calls and return types to inactive-files endpoint
        $this->assertEquals('test.php', $result[0]['name']);
        $this->assertEquals('test.php', $result[1]['name']);
        $this->assertEquals('/path/to/', $result[0]['path']);
        $this->assertEquals('/path/to/', $result[1]['path']);
    }

    public function testSetMarkedFiles()
    {
        $testValues = [
            ['file1.php'],
            ['/path/to/'],
            ['file2.php'],
            ['/path/to/'],
            ['file3.php'],
            ['/path/to/'],
        ];

        $testValueSetsNeeded = 3;

        $statementDummy = $this->prophet->prophesize(PDOStatement::class);
        $statementDummy->bindValue(1, 'file1.php', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->bindValue(2, '/path/to/', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->bindValue(3, 'file2.php', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->bindValue(4, '/path/to/', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->bindValue(5, 'file3.php', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->bindValue(6, '/path/to/', PDO::PARAM_STR)->willReturn(true);
        $statementDummy->execute()->willReturn(true);

        $pdoDummy = $this->prophet->prophesize(PDO::class);
        $pdoDummy
            ->prepare("INSERT INTO marked_files (name, path, date_created) VALUES (?, ?, CURRENT_TIMESTAMP()),(?, ?, CURRENT_TIMESTAMP()),(?, ?, CURRENT_TIMESTAMP());")
            ->willReturn($statementDummy->reveal());

        $this->system_under_test = new MyVoiceCommModel($pdoDummy->reveal());

        $result = $this->system_under_test->setMarkedFiles($testValues, $testValueSetsNeeded);
        $this->assertTrue($result);
    }

    public function testGetInactiveFiles()
    {
        $statementDummy = $this->prophet->prophesize(PDOStatement::class);
        $statementDummy->execute();
        $statementDummy->fetchAll()->willReturn([['name' => 'test.php', 'path' => '/path/to/'], ['name' => 'test.php', 'path' => '/path/to/']]);

        $pdoDummy = $this->prophet->prophesize(PDO::class);
        $pdoDummy
            ->prepare("SELECT marked.name, marked.path FROM marked_files as marked LEFT JOIN active_files as active ON marked.name = active.name AND marked.path = active.path WHERE active.path IS NULL AND active.name IS NULL GROUP BY name, path;")
            ->willReturn($statementDummy->reveal());

        $this->system_under_test = new MyVoiceCommModel($pdoDummy->reveal());

        $result = $this->system_under_test->getInactiveFiles();

        // Similar method calls and return types to get-marked-files endpoint
        $this->assertEquals('test.php', $result[0]['name']);
        $this->assertEquals('test.php', $result[1]['name']);
        $this->assertEquals('/path/to/', $result[0]['path']);
        $this->assertEquals('/path/to/', $result[1]['path']);
    }
}
