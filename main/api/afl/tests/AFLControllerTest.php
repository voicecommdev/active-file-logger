<?php
use PHPUnit\Framework\TestCase;
use AFLControllers\ActiveFileLoggerController;

class AFLControllerTest extends TestCase
{
    public $system_under_test;
    public $prophet;

    public function setUp(): void
    {
        $this->prophet = new \Prophecy\Prophet();
        $loggerDummy = $this->prophet->prophesize(\Monolog\Logger::class);

        $modelDummy = $this->prophet->prophesize(\AFLModels\MyVoiceCommModel::class);
        $modelDummy->logAccessToFile('file.php', '/path/to/')->willReturn(true);
        $modelDummy->getMarkedFiles()->willReturn(['test' => 'test1']);
        $modelDummy->setMarkedFiles(['test.php', '/test/path/'], 1)->willReturn(true);
        $modelDummy->getInactiveFiles()->willReturn(['test' => 'test1']);

        $container = $this->prophet->prophesize(\Psr\Container\ContainerInterface::class);
        $container->get('MyVoiceCommModel')->willReturn($modelDummy->reveal(), null);
        $container->get('Logger')->willReturn($loggerDummy->reveal(), null);

        $this->system_under_test = new ActiveFileLoggerController($container->reveal());
    }

    public function testLogAccessToFiles()
    {
        $requestDummy = $this->prophet->prophesize(\Slim\Psr7\Request::class);
        $requestDummy->getParsedBody()->willReturn(['name' => 'file.php', 'path' => '/path/to/']);

        $response = new \Slim\Psr7\Response();
        $result = $this->system_under_test->logAccessToFile($requestDummy->reveal(), $response, []);

        $this->assertEquals('application/json', $result->getHeaderLine('Content-Type'));
        $this->assertEquals(200, $result->getStatusCode());
    }

    public function testGetMarkedFiles()
    {
        $requestDummy = $this->prophet->prophesize(\Slim\Psr7\Request::class);
        $response = new \Slim\Psr7\Response();

        $result = $this->system_under_test->getMarkedFiles($requestDummy->reveal(), $response, []);

        $this->assertEquals('application/json', $result->getHeaderLine('Content-Type'));
        $this->assertEquals(200, $result->getStatusCode());
    }

    public function testSetMarkedFiles()
    {
        $requestDummy = $this->prophet->prophesize(\Slim\Psr7\Request::class);
        $requestDummy->getParsedBody()->willReturn(['/path/to/file.php', '/path/to/another/file.php']);

        $response = new \Slim\Psr7\Response();
        $result = $this->system_under_test->setMarkedFiles($requestDummy->reveal(), $response, []);

        $this->assertEquals('application/json', $result->getHeaderLine('Content-Type'));
        $this->assertEquals(200, $result->getStatusCode());
    }

    public function testGetInactiveFiles()
    {
        $requestDummy = $this->prophet->prophesize(\Slim\Psr7\Request::class);
        $requestDummy->getParsedBody()->willReturn(['name' => 'file.php', 'path' => '/path/to/']);

        $response = new \Slim\Psr7\Response();
        $result = $this->system_under_test->getInactiveFiles($requestDummy->reveal(), $response, []);

        $this->assertEquals('application/json', $result->getHeaderLine('Content-Type'));
        $this->assertEquals(200, $result->getStatusCode());
    }
}
