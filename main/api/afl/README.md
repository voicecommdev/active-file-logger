#Active File Logger
##(AFL or Tombstone or App2)

The purpose of this light API is to access two database tables.
There are four endpoints associated with the main functionality of the app.

### Definitions:

#### Marked Files 
 - Marked Files are files that have had the "tombstone" script added to the top of them. The actual marking process takes place in App 1.

#### Active Files
 - Active Files are PHP files that have been "marked" (see above) and have called the /log-access endpoint to show that they have been accessed. These files are still in use.

#### Inactive Files
 - Inactive Files are files that have been marked but never called the /log-access endpoint. These files are targeted for removal by App 3.

### Endpoints:
####`/log-access`
Designed to accept a file name and file path and log them in the Active Files table.
This endpoint will be what the individual PHP files use to callout that they have been accessed. 

####`/set-marked-files`
'Set marked files' accepts a bulk of file names and associated file paths. 
This bulk list of file names and paths will be bulk inserted into the Marked Files table.

####`/get-marked-files`
Gets a list of the files (and their respective paths) that have already been marked. 

####`/inactive-files`
Gets a list of files (and their respective paths) that have been marked but have never been listed as active.

### Database Structure for Development:

    CREATE DATABASE myvoicecomm;
    USE myvoicecomm;

    CREATE TABLE `active_files` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `path` varchar(255) NOT NULL,
      `date_created` datetime NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id_UNIQUE` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

    CREATE TABLE `marked_files` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `path` varchar(255) NOT NULL,
      `date_created` datetime NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `id_UNIQUE` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

### Queries to set up a database user:

    CREATE USER 'user'@'%' IDENTIFIED BY 'password';

    GRANT ALL PRIVILEGES ON *.* TO 'user'@'%';
    
### Docker Setup

The Docker setup is fairly routine. The Docker Compose is straightforward:

    version: "3.7"
    services:
      afl-api:
        build: main/api/.
        environment:
          MYSQL_HOSTNAME: localhost
          MYSQL_PORT: 3306
          MYSQL_USER: 
          MYSQL_PASSWORD: 
          MYSQL_DATABASE: myvoicecomm
          ERROR_LOG: '/var/log/afl-app/error.log'
        volumes:
          - ./main/api/afl:/var/www/html
        ports:
          - "88:80"
          
Currently the environment variables are not auto-populated into the .env file.