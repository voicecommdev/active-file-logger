#!/bin/bash

cp /var/www/html/.env.dist /var/www/html/.env
sed -i -e "s@ENV_MYSQL_HOSTNAME@$MYSQL_HOSTNAME@" /var/www/html/.env
sed -i -e "s@ENV_MYSQL_PORT@$MYSQL_PORT@" /var/www/html/.env
sed -i -e "s@ENV_MYSQL_USER@$MYSQL_USER@" /var/www/html/.env
sed -i -e "s@ENV_MYSQL_PASSWORD@$MYSQL_PASSWORD@" /var/www/html/.env
sed -i -e "s@ENV_MYSQL_DATABASE@$MYSQL_DATABASE@" /var/www/html/.env

cd /var/www/html
#composer install
#a2enmod rewrite headers
#a2ensite afl.conf
#a2dissite 000-default.conf
#service apache2 reload
#service apache2 restart

tail -f /var/log/apache2/error.log